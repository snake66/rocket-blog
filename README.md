# A simple blog engine made with Rocket, Diesel and Bart

This is essentially a toy project to learn [Rocket](https://rocket.rs) and [Diesel](https://diesel.rs). Also I'm using [Bart](https://crates.io/crates/bart) for templates, because it seemed simple, and I like the name.

## Getting started

### Using Vagrant

You need to inatsll both [Vagrant] and [VirtualBox] unless you already have them installed. Then from the directory where you have cloned the rocket-blog type:

    % vagrant up

This will install, prepare and start the virtual machine where you will run the program. This will take some time the first time you do it. Once it is done, you can log in to the virtual machine by typing:

    % vagrant ssh

In the virtual machine, do the following:

    % cd /vagrant
    % echo "DATABASE_URL=postgres://vagrant@localhost/rocket_blog" > .env
    % diesel setup

The application is now ready, and can be started like this:

    % cargo run

Now you can open http://localhost:8000 in your web browser.

### Using a local developer setup

First make sure you have PostgreSQL installed. We don't support any other database engine for now. You also need the nightly Rust compiler for now. To set the current project to use the nightly compiler, run:

    % rustup override nightly-2017-12-21

Then add a database url into a `.env`-file  that is to be located in the root of the source directory:

    % echo DATABASE_URL=postgres://dbuser:password@localhost/dbname > .env

Then create the user and database specified above:

    % createuser -U postgres --createdb --login -P dbuser

Replace `dbuser`, `dbname` and `password` with values that makes sense to you. You will be prompted for the password when creating the user.

Next install `diesel_cli`:

    % cargo install diesel_cli

Then run the diesel setup to create the database and run the migrations:

    % ~/.cargo/bin/diesel setup

Check that everything is working:

    % cargo test

Run the engine:

    % cargo run

## License

This project is provided as free software under the GNU AGPL v3, see LICENSE for details.

[Vagrant]: https://www.vagrantup.com
[VirtualBox]: https://www.virtualbox.org/
