use rocket;
use rocket::request::Form;
use rocket::response::{Flash, Redirect};
use utils;

#[derive(BartDisplay)]
#[template = "templates/new_user.html"]
pub struct NewUserTemplate {
    user: ::models::NewUser
}

implement_responder_for!(NewUserTemplate);

impl NewUserTemplate {
    pub fn realname(&self) -> String {
        self.user.realname.as_ref().unwrap_or(&String::new()).clone()
    }

    pub fn email(&self) -> String {
        self.user.email.as_ref().unwrap_or(&String::new()).clone()
    }
}

#[get("/new", format = "text/html")]
fn new() -> utils::Page<NewUserTemplate> {
    utils::Page {
        title: String::from("New user"),
        flash: None,
        content: NewUserTemplate {
            user: Default::default()
        }
    }
}

#[post("/create", data="<user>")]
fn create(user: Form<::models::NewUser>, conn: utils::DbConn) -> Flash<Redirect> {
    match ::models::User::create(user.get(), conn) {
        Ok(_) => Flash::success(Redirect::to("/"), "User successfully created!"),
        Err(_) => Flash::error(Redirect::to("/"), "Could not create user!")
    }
}

pub fn routes() -> Vec<rocket::Route> {
    routes![new, create]
}
