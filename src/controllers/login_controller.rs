use rocket;
use rocket::request::Form;
use rocket::response::{Flash, Redirect};
use models;
use utils;

#[derive(BartDisplay)]
#[template = "templates/login.html"]
pub struct LoginTemplate;

implement_responder_for!(LoginTemplate);

#[get("/", format="text/html")]
pub fn new(flash: Option<rocket::request::FlashMessage>) -> utils::Page<LoginTemplate> {
    utils::Page {
        title: String::from("Log in"),
        flash: flash.map_or(None, |f| Some(f.msg().to_string())),
        content: LoginTemplate{}
    }
}

#[derive(FromForm)]
pub struct LoginForm {
    email: String,
    password: String,
}

#[post("/create", data="<login>")]
pub fn create(login: Form<LoginForm>, conn: utils::DbConn) -> Flash<Redirect> {
    let login = login.get();
    if let Ok(user) = models::User::by_email(&login.email, conn) {
        if user.password == login.password {
            return Flash::success(Redirect::to("/"), format!("{} logged in successfully", user.realname.or(Some(user.username)).unwrap()));
        }
    }

    Flash::error(Redirect::to("/login"), "Invalid email or passoword!")
}

pub fn routes() -> Vec<rocket::Route> {
    routes![new, create]
}
