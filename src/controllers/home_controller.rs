use ::{models, utils};
use rocket;
use rocket_contrib::Json;
use std::path::PathBuf;
use posts_controller::ShowPostTemplate;

#[derive(BartDisplay, Serialize)]
#[template = "templates/index.html"]
struct IndexTemplate {
    posts: Vec<ShowPostTemplate>
}

implement_responder_for!(IndexTemplate);

#[get("/", format = "text/html")]
fn index(flash: Option<rocket::request::FlashMessage>, conn: utils::DbConn) -> utils::Page<IndexTemplate> {
    utils::Page {
        title: String::from("Bloggen"),
        flash: flash.map_or(None, |f| Some(f.msg().to_string())),
        content: IndexTemplate {
            posts: models::Post::get_all(conn).into_iter()
                .map(|p| ShowPostTemplate { post: p })
                .collect()
        }
    }
}

#[get("/", format = "application/json")]
fn index_json(conn: utils::DbConn) -> Json<Vec<models::Post>> {
    Json(models::Post::get_all(conn))
}

//
// Serve files from the public directory if no routes matches.
//
#[get("/<file..>", rank = 99)]
fn public_file(file: PathBuf) -> Option<rocket::response::NamedFile> {
    rocket::response::NamedFile::open(PathBuf::from("public/").join(file)).ok()
}

pub fn routes() -> Vec<rocket::Route> {
    routes![
        index,
        index_json,
        public_file
    ]
}
