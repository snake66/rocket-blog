#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

#[macro_use] extern crate bart_derive;
#[macro_use] extern crate diesel;
#[macro_use] extern crate serde_derive;

extern crate comrak;
extern crate dotenv;
extern crate r2d2;
extern crate r2d2_diesel;
extern crate rocket;
extern crate rocket_contrib;

#[macro_use] mod utils;
mod models;
mod schema;
mod controllers;
use controllers::{
    home_controller,
    login_controller,
    posts_controller,
    users_controller
};

fn main() {
    if let Ok(dburl) = dotenv::var("DATABASE_URL") {
        rocket::ignite()
            .manage(utils::init_db_pool(&dburl))
            .mount("/", home_controller::routes())
            .mount("/posts", posts_controller::routes())
            .mount("/users", users_controller::routes())
            .mount("/login", login_controller::routes())
            .launch();
    }
    else {
        eprintln!("Error: No database specified, make sure the DATABASE_URL env var is set.");
    };
}
