-- Create users table
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR NOT NULL,
  realname VARCHAR,
  email VARCHAR,
  password VARCHAR NOT NULL
)
